subject_number = int(input('შეიყვანეთ საგნების რაოდენობა: ')) # ვიღებთ უსერიდან საგნების რაოდენობას
subject_grades = [int(input()) for i in range(subject_number)] # შემგომ შეფასებას და ვინახავთ list-ში

print(f'max: {max(subject_grades)}') # ვიპოვით max და დავბეჭდავთ
print(f'min: {min(subject_grades)}') # აქაც იგივე
print(f'average: {sum(subject_grades)/subject_number}') # ვიპოვით ჯამს და შევაფარდებთ რაოდენობასთან

text = input("შეიყანეთ ტექსტი: ")
if text == text[::-1]:  # text[::-1] ტექსტის რევერსი
    print('არის პალინდრომი')
else:
    print('არ არის პალინდრომი')
